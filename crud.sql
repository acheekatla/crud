-- creating db
create database firstdb;

-- reading db
use firstdb;

-- updating db
create table company(companyname VARCHAR(30), id int(25),city varchar(50), numberofemp int);

-- deleting db
 DROP DATABASE Gang;
 
 
-- creating table 
create table Gang(name VARCHAR(30), id int(25),college varchar(50), age int);

-- reading table
desc Gang;

-- updating table
ALTER TABLE Gang  ADD COLUMN phonenumber int;

-- deleting table
drop table Gang;


-- creating record
INSERT INTO Gang values ('harshi',1001,'au',21),('appu',1002,'au',21),('gowthami',1003,'aucew',20),('neelima',1004,'au',21);

-- reading record
SELECT * FROM Gang;

-- updating record
UPDATE Gang SET name = 'aparna' WHERE id = 1002;

-- deleting record
DELETE FROM Gang WHERE id = 1002;





